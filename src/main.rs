use std::time::Duration;

use bevy::prelude::*;
use bevy_dynamic_gravity_2d::prelude::*;
use bevy_rapier2d::prelude::*;

// ------------------------------------------------------------------------------------------------

fn main() {
    App::new()
        .add_plugins((
            DefaultPlugins,
            DynamicGravityPlugin,
            RapierPhysicsPlugin::<NoUserData>::default().in_fixed_schedule(),
            RapierDebugRenderPlugin::default(),
        ))
        .add_systems(Startup, setup)
        .add_systems(PreUpdate, update)
        .run();
}

fn update(
    keys_in: Res<ButtonInput<KeyCode>>,
    mut mover_query: Query<(&mut GroundMover, &mut GroundJumper)>,
) {
    let (mut mover, mut jumper) = mover_query.single_mut();

    let mut mov = Vec2::ZERO;
    if keys_in.pressed(KeyCode::KeyD) {
        mov.x += 1.0;
    }
    if keys_in.pressed(KeyCode::KeyA) {
        mov.x -= 1.0;
    }
    if keys_in.pressed(KeyCode::KeyW) {
        mov.y += 1.0;
    }
    if keys_in.pressed(KeyCode::KeyS) {
        mov.y -= 1.0;
    }
    mover.set_absolute_movement(mov);
    jumper.set_jumping(keys_in.pressed(KeyCode::Space));
}

fn setup(
    mut commands: Commands,
    mut phys_dbg_render: ResMut<DebugRenderContext>,
    mut rapier_config: ResMut<RapierConfiguration>,
    mut time: ResMut<Time<Virtual>>,
    mut fixed_time: ResMut<Time<Fixed>>,
) {
    // set a max timestep for the program
    time.set_max_delta(Duration::from_secs_f64(0.1));

    // determine the fixed and physics timestep delta
    let phys_timestep_delta: Duration = Duration::from_secs_f64(1.0 / 60.0);
    fixed_time.set_timestep(phys_timestep_delta);

    // disable constant gravity because we use dynamic gravity
    rapier_config.gravity.x = 0.0;
    rapier_config.gravity.y = 0.0;

    // force rapier config to use a fixed time step since its running in the fixed update
    rapier_config.timestep_mode = TimestepMode::Fixed {
        dt: fixed_time.timestep().as_secs_f32(),
        substeps: 1,
    };

    // configure the renderer
    phys_dbg_render.enabled = true;
    phys_dbg_render.pipeline.style.rigid_body_axes_length = 1.0;

    // set the planets positions
    let planet_pos = Vec3::new(0.0, -15.0, 0.0);

    // create camera
    commands.spawn(Camera2dBundle {
        transform: Transform::from_translation(planet_pos),
        projection: OrthographicProjection {
            near: -1000.0,
            far: 1000.0,
            scale: 0.05,
            ..default()
        },
        ..default()
    });

    // spawn player
    commands.spawn((
        TransformInterpolation::default(),
        CharacterControllerBundle::default(),
    ));

    // spawn circle planet
    commands.spawn((
        SpatialBundle::from_transform(Transform::from_translation(planet_pos + Vec3::X * 12.0)),
        RigidBody::Fixed,
        DynamicGravityAttractor {
            gravity_falloff_extension: 1000.0,
            ..Default::default()
        },
        Collider::ball(7.5),
    ));

    // spawn square planet
    commands.spawn((
        SpatialBundle::from_transform(Transform::from_translation(planet_pos - Vec3::X * 12.0)),
        RigidBody::Fixed,
        DynamicGravityAttractor {
            gravity_falloff_extension: 1000.0,
            ..Default::default()
        },
        Collider::cuboid(7.5, 7.5),
    ));

    // spawn props
    let ctr = planet_pos - Vec3::Y * 10.0;
    let count = 50;
    let spacing = 2.0;
    for i in 0..count {
        let off = i as f32 * spacing - count as f32 * spacing * 0.5;
        let pos = Vec2::new(ctr.x + off, ctr.y);
        let size = 0.75 + (i as f32 % 3.0) * 0.25;
        let col = if i % 2 == 0 {
            Collider::cuboid(size * 1.5, size * 0.75)
        } else {
            Collider::ball(size * 0.75)
        };
        commands.spawn((
            TransformInterpolation::default(),
            SpatialBundle::from_transform(Transform::from_translation(pos.extend(0.0))),
            RigidBody::Dynamic,
            Velocity::zero(),
            col,
            DynamicGravity::default(),
        ));
    }

    // fin.
    println!("Example Initialized!");
}
