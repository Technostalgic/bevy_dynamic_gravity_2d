use bevy::prelude::*;
use bevy_rapier2d::prelude::*;

use crate::core::{math_utils::*, physics::dynamic_gravity::DynamicGravity};

// System Sets: ------------------------------------------------------------------------------------

/// a system set the includes any systems that take part in calculating whether an entity with a
/// [`Grounded`] component is on the ground or not
#[derive(SystemSet, Debug, Hash, Clone, Copy, PartialEq, Eq)]
pub struct GroundedRecalculation;

// Grounded Plugin: --------------------------------------------------------------------------------

pub struct GroundedPlugin;

impl Plugin for GroundedPlugin {
    fn build(&self, app: &mut App) {
        app.configure_sets(
            FixedUpdate,
            GroundedRecalculation.after(PhysicsSet::Writeback),
        )
        .add_systems(FixedUpdate, handle_grounded.in_set(GroundedRecalculation));
    }
}

// Grounded Component: -----------------------------------------------------------------------------

#[derive(Component, Clone, Copy)]
pub struct Grounded {
    /// the minimum flatness of a slope that the entity will consider to be ground. a threshold of
    /// 1 will only consider perfectly flat ground to be ground, while a threshold of 0 would
    /// consider a completely vertical cliff to be ground
    pub slope_threshold: f32,
    /// how long the entity can be in the air for before being considered not in the ground anymore.
    /// this can act as a grace period to allow characters who time their jump incorrectly to still
    /// be able to jump even if they are no longer on the platform. It can also be useful for
    /// objects with high restitution, so that they can be considered on the ground if they bounce
    /// a bit after an impact
    pub max_coyote_time: f32,
    coyote_time: f32,
    just_unset: bool,
    last_grounded_gravity: Vec2,
    was_on_ground: bool,
    _ground_velocity: Vec2,
}

impl Default for Grounded {
    fn default() -> Self {
        Self {
            slope_threshold: 0.5,
            max_coyote_time: 0.2,
            coyote_time: default(),
            just_unset: false,
            last_grounded_gravity: default(),
            was_on_ground: false,
            _ground_velocity: default(),
        }
    }
}

impl Grounded {
    /// whether or not the entity is considered to be currently on the ground, given the properties
    /// specified within this component
    pub fn on_ground(&self) -> bool {
        self.coyote_time > 0.0
    }
    /// force the entity to be not considered on the ground, it will remain non-grounded until it
    /// comes in contact with the ground again (which could be the very next frame)
    pub fn set_ungrounded(&mut self) {
        if self.coyote_time > 0.0 {
            self.coyote_time = 0.0;
            self.just_unset = true;
        }
    }
}

fn handle_grounded(
    time: Res<Time>,
    collisions: Res<RapierContext>,
    mut grounded_query: Query<(
        Entity,
        &mut Grounded,
        &DynamicGravity,
        &mut Velocity,
    )>,
) {
    let dt = time.delta_seconds();
    for (ent, mut grounded, gravity, mut vel) in &mut grounded_query {
        let total_gravity = gravity.total_gravity();
        let up_axis = -total_gravity.normalize_or_zero();
        if grounded.on_ground() {
            let last_grav = grounded.last_grounded_gravity;
            grounded.last_grounded_gravity = total_gravity;
            // wrap forward velocity when ground direction is changing so you don't fly off the edge
            // when moving forward
            if grounded.was_on_ground {
                let last_up_axis = -last_grav.normalize_or_zero();
                let last_forward_axis = last_up_axis.rotate_cw();
                let forward_axis = up_axis.rotate_cw();
                let last_up_vel = vel.linvel.dot(last_up_axis);
                let last_forward_vel = vel.linvel.dot(last_forward_axis);
                vel.linvel = last_forward_vel * forward_axis + last_up_vel * up_axis;
            }
        }
        grounded.was_on_ground = grounded.on_ground();
        // deduct coyote time so that is no longer considered on ground after threshold
        grounded.coyote_time -= dt;
        // look for ground candidates in collisions
        for collision in collisions.contact_pairs_with(ent) {
            // contact manifolds can exist even if there is no contact, so we need to ensure
            // that there is an actual collision contact
            if collision.has_any_active_contacts() {
                for manifold in collision.manifolds() {
                    // use the proper normal, since the grounded entity could be either entity 1 or 2
                    // involved in the collision, it won't necessarily always be entity 1
                    let normal = if collision.collider1() == ent {
                        manifold.normal() * -1.0
                    } else {
                        manifold.normal()
                    };
                    // currently on ground if contact normal meets slope threshold
                    let dot = up_axis.dot(normal);
                    if dot >= grounded.slope_threshold {
                        grounded.coyote_time = grounded.max_coyote_time;
                    }
                }
            }
        }
    }
}
