use bevy::prelude::*;
use bevy_rapier2d::prelude::*;

use crate::core::{
    math_utils::*,
    physics::dynamic_gravity::{DynamicGravity, DynamicGravityCalculation},
};

use super::ground_movement::GroundMovement;

// Gravity Orient Plugin: --------------------------------------------------------------------------

pub struct GravityOrientPlugin;

impl Plugin for GravityOrientPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(
            FixedUpdate,
            orient_to_gravity
                .after(DynamicGravityCalculation)
                .after(GroundMovement)
                .before(PhysicsSet::SyncBackend),
        );
    }
}

// Orient to Gravity Component: --------------------------------------------------------------------

#[derive(Component, Debug, Clone, Copy)]
pub struct OrientToGravity {
    /// the speed at which the body rotates to reach it's target in radians per second
    pub rot_speed: f32,
    /// the target angle offset of the body
    pub offset: f32,
}

impl Default for OrientToGravity {
    fn default() -> Self {
        Self {
            rot_speed: TWO_PI,
            offset: HALF_PI,
        }
    }
}

/// system that orients the object to it's dynamic gravity
fn orient_to_gravity(
    time: Res<Time>,
    mut query: Query<(&mut Transform, &OrientToGravity, &DynamicGravity)>,
) {
    let dt = time.delta_seconds();
    for (mut trans, orient, grav) in &mut query {
        let total_gravity = grav.total_gravity();
        if total_gravity.is_zero() {
            continue;
        }
        let target = total_gravity.to_angle() + orient.offset;
        let rotation_dif = signed_angle_dif(trans.rotation2d(), target);
        // if need to rotate more than 90 degrees, flip instead
        if rotation_dif.abs() > HALF_PI + 0.1 {
            trans.rotate2d(PI);
            continue;
        }
        let max_delta = orient.rot_speed * dt;
        if rotation_dif.abs() <= max_delta {
            trans.set_rotation2d(target);
        } else {
            trans.rotate2d(max_delta * rotation_dif.signum());
        }
    }
}
