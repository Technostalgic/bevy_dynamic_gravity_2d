use bevy::prelude::*;
use bevy_rapier2d::prelude::*;

use crate::core::{
    math_utils::{IsZero, RotateVecQuarterAngle, Rotation2d},
    physics::dynamic_gravity::{ApplyDynamicGravity, DynamicGravity},
};

use super::grounded::Grounded;

// System Sets: ------------------------------------------------------------------------------------

/// a system set that includes systems which involve translating a movement action into the forces
/// or accelerations onto the physics body
#[derive(SystemSet, Debug, Hash, Clone, Copy, PartialEq, Eq)]
pub struct GroundMovement;

// Ground Movement Plugin: -------------------------------------------------------------------------

pub struct GroundMovementPlugin;

impl Plugin for GroundMovementPlugin {
    fn build(&self, app: &mut App) {
        app.configure_sets(
            FixedUpdate,
            GroundMovement
                .after(ApplyDynamicGravity)
                .before(PhysicsSet::SyncBackend),
        )
        .add_systems(
            FixedUpdate,
            (ground_movement, ground_jumping)
                .chain()
                .in_set(GroundMovement),
        );
    }
}

// Ground Mover Component: -------------------------------------------------------------------------

#[derive(Component, Debug, Clone, Copy)]
pub struct GroundMover {
    /// the max speed at which the mover can move at, in units per second
    pub move_speed: f32,
    /// how quickly they can accelerate to their max speed, a value of 1 means it takes one second,
    /// at 2 it takes a 1/2 second, etc
    pub acceleration_factor: f32,
    /// whether or not the mover is currently using the movement field in terms of global space
    absolute_movement: bool,
    movement: Vec2,
    cached_friction_coeff: Option<f32>,
}

impl Default for GroundMover {
    fn default() -> Self {
        Self {
            move_speed: 10.0,
            acceleration_factor: 5.0,
            absolute_movement: default(),
            movement: default(),
            cached_friction_coeff: default(),
        }
    }
}

impl GroundMover {
    /// set the relative horizontal movement of the ground mover, with respect to their transform's
    /// relative forward axis
    pub fn set_relative_movement(&mut self, mov: f32) {
        self.absolute_movement = false;
        self.movement.x = mov;
    }
    /// set the movement vector of the entity to accelerate in a direction relative to the game's
    /// global space
    pub fn set_absolute_movement(&mut self, mov: Vec2) {
        self.absolute_movement = true;
        self.movement = mov;
    }
}

/// translate hero movement data into actual physics based motion
fn ground_movement(
    time: Res<Time>,
    mut hero_query: Query<(
        &GlobalTransform,
        &mut GroundMover,
        &Grounded,
        &DynamicGravity,
        &mut Velocity,
        &mut Friction,
    )>,
) {
    let dt = time.delta_seconds();
    for (glob_trans, mut mover, grounded, grav, mut vel, mut fric) in &mut hero_query {
        // cache friction coefficient on first step
        if mover.cached_friction_coeff.is_none() {
            mover.cached_friction_coeff = Some(fric.coefficient);
        }
        let hero_fric_coef = mover.cached_friction_coeff.unwrap();

        // calculate the forward direction based on the gravitational field
        let total_gravity = grav.total_gravity();
        let forward_axis = if total_gravity.is_zero() {
            Vec2::from_angle(glob_trans.rotation2d())
        } else {
            total_gravity.normalize_or_zero().rotate_ccw()
        };
        let up_axis = forward_axis.rotate_ccw();

        // calculate the initial forward and up vel
        let mut forward_vel = vel.linvel.dot(forward_axis);
        let up_vel = vel.linvel.dot(up_axis);

        let mut movement = mover.movement.x;

        // translate absolute movement into relative movement if applicable
        if mover.absolute_movement {
            let fwd_dot = forward_axis.dot(mover.movement);
            // the dot threshold here represents a value of slightly less than a half
            // rotation. So any absolute movement vector within more than 75 degrees of
            // the relative forward axis of the entity will cause the entity to move forward
            let dot_thresh = 0.258819;
            movement = if fwd_dot >= dot_thresh {
                1.0
            } else if fwd_dot <= -dot_thresh {
                -1.0
            } else {
                0.0
            };
        }

        let targ_vel = mover.move_speed * movement.clamp(-1.0, 1.0);
        let accel = mover.acceleration_factor * mover.move_speed;

        // enable or disable friction based on if hero is moving
        if movement.abs() > 0.0 {
            fric.coefficient = 0.0;
        } else {
            fric.coefficient = hero_fric_coef;
        }

        // enforce max speed
        if forward_vel.abs() > targ_vel.abs() {
            let dif_vel_x = targ_vel - forward_vel;
            let acc_x = accel * dif_vel_x.signum() * dt;
            if grounded.on_ground() {
                if forward_vel.signum() != (forward_vel + acc_x).signum() {
                    forward_vel = 0.0;
                } else {
                    forward_vel += acc_x;
                }
            }
            fric.coefficient = hero_fric_coef;

        // move
        } else {
            // enable friction if it helps accelerate toward target velocity
            if forward_vel != 0.0 && targ_vel != 0.0 && forward_vel.signum() != targ_vel.signum() {
                fric.coefficient = hero_fric_coef;
            }
            // use acceleration factor to accelerate toward target velocity
            let acc_x = accel * movement * dt;
            if (forward_vel + acc_x).abs() >= targ_vel.abs() {
                forward_vel = targ_vel;
            } else {
                forward_vel += acc_x;
            }
        }

        // re-project relative velocity onto euclidean coordinates
        vel.linvel = forward_vel * forward_axis + up_vel * up_axis;
    }
}

// Ground Jumper Component: ------------------------------------------------------------------------

#[derive(Component, Debug, Clone, Copy)]
pub struct GroundJumper {
    pub jump_power: f32,
    pub jump_sustain: f32,
    is_jumping: bool,
    was_jumping: bool,
    is_jump_ascending: bool,
    _downward_movement: f32,
}

impl Default for GroundJumper {
    fn default() -> Self {
        Self {
            jump_power: 12.0,
            jump_sustain: 0.55,
            is_jumping: default(),
            was_jumping: default(),
            is_jump_ascending: default(),
            _downward_movement: default(),
        }
    }
}

impl GroundJumper {
    pub fn set_jumping(&mut self, is_jumping: bool) {
        self.is_jumping = is_jumping;
    }
}

fn ground_jumping(
    time: Res<Time>,
    mut hero_query: Query<(
        &mut GroundJumper,
        &mut Grounded,
        &mut DynamicGravity,
        Option<&GravityScale>,
        &mut Velocity,
    )>,
) {
    let dt = time.delta_seconds();
    for (mut jumper, mut grounded, mut gravity, grav_scl, mut vel) in &mut hero_query {
        // handle jumping
        let up_axis = -gravity.total_gravity().normalize_or_zero();
        let cur_jump_vel = vel.linvel.dot(up_axis);
        if jumper.is_jumping {
            // initial jump
            if grounded.on_ground() && !jumper.was_jumping {
                let targ_jump_vel = jumper.jump_power;
                let jump_add = if cur_jump_vel < targ_jump_vel {
                    targ_jump_vel - cur_jump_vel
                } else {
                    0.0
                };
                vel.linvel += up_axis * jump_add;
                jumper.is_jump_ascending = true;
                grounded.set_ungrounded();
                gravity.suspend_recalc();
            // jump sustain
            } else if jumper.is_jump_ascending {
                if cur_jump_vel < jumper.jump_power * 0.2 {
                    jumper.is_jump_ascending = false;
                }
                let scl = grav_scl.map_or(1.0, |g| g.0);
                vel.linvel += -gravity.total_gravity() * dt * jumper.jump_sustain * scl;
                gravity.suspend_recalc();
            }
        } else {
            jumper.is_jump_ascending = false;
        }
        jumper.was_jumping = jumper.is_jumping;
    }
}
