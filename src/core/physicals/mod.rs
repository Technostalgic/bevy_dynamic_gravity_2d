use bevy::{
    app::Plugin,
    ecs::bundle::Bundle,
    math::Vec2,
    transform::components::{GlobalTransform, Transform},
};
use bevy_rapier2d::prelude::*;

use self::{
    gravity_orient::OrientToGravity,
    ground_movement::{GroundJumper, GroundMover},
    grounded::Grounded,
};

use super::physics::dynamic_gravity::DynamicGravity;

pub mod gravity_orient;
pub mod ground_movement;
pub mod grounded;

// Physicals Plugin: -------------------------------------------------------------------------------

pub struct PhysicalsPlugin;

impl Plugin for PhysicalsPlugin {
    fn build(&self, app: &mut bevy::prelude::App) {
        app.add_plugins((
            gravity_orient::GravityOrientPlugin,
            grounded::GroundedPlugin,
            ground_movement::GroundMovementPlugin,
        ));
    }
}

// Bundles: ----------------------------------------------------------------------------------------

/// a bundle that includes the minimum required components for an entity who can move and jump
/// on the ground with dynamic gravity
#[derive(Bundle, Clone)]
pub struct BasicGroundMoverBundle {
    pub transform: Transform,
    pub global_trans: GlobalTransform,
    pub rigid_body: RigidBody,
    pub collider: Collider,
    pub dynamic_grav: DynamicGravity,
    pub grounded: Grounded,
    pub mover: GroundMover,
    pub jumper: GroundJumper,
}

impl Default for BasicGroundMoverBundle {
    fn default() -> Self {
        Self {
            transform: Default::default(),
            global_trans: Default::default(),
            rigid_body: RigidBody::Dynamic,
            collider: Collider::capsule(Vec2::new(0.0, 0.75), Vec2::new(0.0, -0.75), 0.75),
            dynamic_grav: Default::default(),
            grounded: Default::default(),
            mover: Default::default(),
            jumper: Default::default(),
        }
    }
}

/// a more robust than the basic ground mover bundle that includes all the components needed to
/// make the entity feel like a proper character controller
#[derive(Bundle, Clone)]
pub struct CharacterControllerBundle {
    pub transform: Transform,
    pub global_trans: GlobalTransform,
    pub rigid_body: RigidBody,
    pub velocity: Velocity,
    pub collider: Collider,
    pub friction: Friction,
    pub dynamic_grav: DynamicGravity,
    pub grav_scale: GravityScale,
    pub grounded: Grounded,
    pub gravity_orient: OrientToGravity,
    pub mover: GroundMover,
    pub jumper: GroundJumper,
    pub locked_axes: LockedAxes,
}

impl Default for CharacterControllerBundle {
    fn default() -> Self {
        Self {
            transform: Default::default(),
            global_trans: Default::default(),
            rigid_body: RigidBody::Dynamic,
            velocity: Velocity::zero(),
            collider: Collider::capsule(Vec2::new(0.0, 0.75), Vec2::new(0.0, -0.75), 0.75),
            friction: Friction::new(1.0),
            dynamic_grav: Default::default(),
            grav_scale: GravityScale(3.0),
            grounded: Default::default(),
            gravity_orient: Default::default(),
            mover: Default::default(),
            jumper: Default::default(),
            locked_axes: LockedAxes::ROTATION_LOCKED,
        }
    }
}
