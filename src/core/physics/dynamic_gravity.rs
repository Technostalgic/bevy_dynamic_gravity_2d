use bevy::{prelude::*, render::primitives::Aabb};
use bevy_rapier2d::prelude::*;

use crate::core::math_utils::*;

// System Sets: ------------------------------------------------------------------------------------

/// system set that holds systems which deal with calculating the gravitational force exerted on
/// any object with the [`DynamicGravity`] component
#[derive(SystemSet, Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct DynamicGravityCalculation;

/// system set that describes systems which apply the dynamic gravity to their corresponding
/// physics bodies
#[derive(SystemSet, Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct ApplyDynamicGravity;

// Dynamic Gravity Plugin: -------------------------------------------------------------------------

pub struct DynamicGravityPlugin;

impl Plugin for DynamicGravityPlugin {
    fn build(&self, app: &mut App) {
        app.configure_sets(
            FixedUpdate,
            DynamicGravityCalculation.before(ApplyDynamicGravity),
        )
        .configure_sets(
            FixedUpdate,
            ApplyDynamicGravity.before(PhysicsSet::SyncBackend),
        )
        .add_systems(
            FixedUpdate,
            (
                (reset_total_gravity, calc_total_gravity)
                    .chain()
                    .in_set(DynamicGravityCalculation),
                apply_dynamic_gravity.in_set(ApplyDynamicGravity),
            ),
        );
    }
}

// Dynamic Gravity Component: ----------------------------------------------------------------------

#[derive(Component, Default, Debug, Clone, Copy)]
pub struct DynamicGravity {
    total_gravity: Vec2,
    current_attractor: Option<Entity>,
    attractor_distance: f32,
    attractor_precedence: u8,
    suspend_recalc: bool,
}

impl DynamicGravity {
    /// the total gravitational acceleration acting on the body
    pub fn total_gravity(&self) -> Vec2 {
        self.total_gravity
    }
    /// the most prominent current gravitational attractor that is effecting the body
    pub fn current_attractor(&self) -> &Option<Entity> {
        &self.current_attractor
    }
    /// signal the component to not recalculate dynamic gravity for this tick
    pub fn suspend_recalc(&mut self) {
        self.suspend_recalc = true;
    }
}

fn reset_total_gravity(mut query: Query<&mut DynamicGravity, With<RigidBody>>) {
    for mut grav in &mut query {
        if grav.suspend_recalc {
            continue;
        }
        grav.total_gravity.x = 0.0;
        grav.total_gravity.y = 0.0;
        grav.current_attractor = None;
        grav.attractor_distance = 0.0;
        grav.attractor_precedence = 0;
    }
}

/// a system to calculate the total amount of gravity acting on the component
fn calc_total_gravity(
    rapier_ctx: Res<RapierContext>,
    mut grav_query: Query<(&GlobalTransform, &mut DynamicGravity)>,
    attractor_query: Query<(
        Entity,
        &GlobalTransform,
        &DynamicGravityAttractor,
        &Collider,
    )>,
) {
    // iterate through each gravitational body
    for (atrct_ent, atrct_glob_trans, atrct, atrct_col) in &attractor_query {
        let atrct_aabb = atrct_col
            .raw
            .0
            .compute_aabb(&atrct_glob_trans.to_isometry());
        let expansion = atrct.gravity_falloff_extension + atrct.gravity_falloff;
        let half_width = atrct_aabb.half_extents().x + expansion;
        let half_height = atrct_aabb.half_extents().y + expansion;
        let center = atrct_aabb.center();

        // spatial query for all dynamic gravity entities potentially within the gravity's range
        // and iterate through each of them
        let query_box = Aabb::from_min_max(
            Vec3::new(center.x - half_width, center.y - half_height, 1000.0),
            Vec3::new(center.x + half_width, center.y + half_height, -1000.0),
        );
        rapier_ctx.colliders_with_aabb_intersecting_aabb(query_box, |col| {
            // only process entities who match the dynamic gravity entity query
            let Ok((glob_trans, mut grav)) = grav_query.get_mut(col) else {
                return true;
            };
            // TODO calcualte proper center of mass
            let mass_center = Vec2::ZERO;
            // dont recalculate if flagged not to
            if grav.suspend_recalc {
                grav.suspend_recalc = false;
                return true;
            }
            let mass_center = glob_trans.transform_point2(mass_center);
            let mass_center_from_atrct = atrct_glob_trans.inverse_transform_point2(mass_center);
            // calculate the gravitational attraction target point, where the gravity will pull to
            let atrct_target = atrct_glob_trans.transform_point2(Vec2::from(
                atrct_col
                    .project_local_point(mass_center_from_atrct, true)
                    .point,
            ));
            // calculate the difference between the two attraction points
            let atrct_dif = atrct_target - mass_center;
            let grav_dist = atrct_dif.length();
            // store info about the current attractor or skip it if it has less precedence than
            // a previous attractor
            if grav.current_attractor().is_none() {
                grav.current_attractor = Some(atrct_ent);
                grav.attractor_distance = grav_dist;
                grav.attractor_precedence = atrct.precedence;
            } else {
                if atrct.precedence < grav.attractor_precedence
                    || grav_dist >= grav.attractor_distance
                {
                    return true;
                }
                grav.current_attractor = Some(atrct_ent);
                grav.attractor_distance = grav_dist;
                grav.attractor_precedence = atrct.precedence;
            }
            // calculate the magnitude of the gravity's attraction from 0 to 1
            let max_grav_falloff = atrct.gravity_falloff_extension + atrct.gravity_falloff;
            let attract_magnitude = if grav_dist <= atrct.gravity_falloff_extension {
                1.0
            } else if grav_dist <= max_grav_falloff {
                let grav_faloff_range = max_grav_falloff - atrct.gravity_falloff_extension;
                1.0 - (grav_dist - atrct.gravity_falloff_extension) / grav_faloff_range
            } else {
                0.0
            };
            // scale the gravity and apply to dynamic gravity entity
            let grav_attraction = attract_magnitude * atrct.max_gravity;
            grav.total_gravity = atrct_dif.normalize_or_zero() * grav_attraction;
            return true;
        });
    }
}

/// system that applies the dynamic gravity to the physics body
fn apply_dynamic_gravity(
    time: Res<Time>,
    mut query: Query<(&mut Velocity, &DynamicGravity, Option<&GravityScale>)>,
) {
    let dt = time.delta_seconds();
    for (mut vel, grav, grav_scl) in &mut query {
        let grav_scl = grav_scl.map_or(1.0, |g| g.0);
        let total_gravity = grav.total_gravity();
        vel.linvel.x += total_gravity.x * grav_scl * dt;
        vel.linvel.y += total_gravity.y * grav_scl * dt;
    }
}

// Gravity Attractor Component: --------------------------------------------------------------------

/// attracts entities with the [`DynamicGravity`] component toward its surface
#[derive(Component, Debug, Clone, Copy)]
pub struct DynamicGravityAttractor {
    /// strongest the gravity can possibly be for a gravitational body
    pub max_gravity: f32,
    /// the distance that it takes to go from the gravity's strongest gravitational pull to no
    /// gravitational pull at all
    pub gravity_falloff: f32,
    /// the distance from the gravitational body's surface that the max gravitational pull is
    /// extended above. For instance, a value of 1 will mean that the max_gravity force will
    /// be exerted on any actors for up to 1 unit away from the suface of this gravitational body,
    /// and after that distance is exceeded, the gravity falloff will start taking effect
    pub gravity_falloff_extension: f32,
    /// attractors with higher precedence will be prioritized over ones will lower precedence.
    /// if multiplle attractors have the same precedence, the closest one will be used
    pub precedence: u8,
}

impl Default for DynamicGravityAttractor {
    fn default() -> Self {
        Self {
            max_gravity: 10.0,
            gravity_falloff: 5.0,
            gravity_falloff_extension: 10.0,
            precedence: 0,
        }
    }
}
