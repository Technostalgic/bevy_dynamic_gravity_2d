use bevy::prelude::*;

pub mod dynamic_gravity;

// Physics Plugin: ---------------------------------------------------------------------------------

pub struct PhysicsPlugin;

impl Plugin for PhysicsPlugin {
    fn build(&self, app: &mut bevy::prelude::App) {
        app.add_plugins((dynamic_gravity::DynamicGravityPlugin,));
    }
}
