pub mod math_utils;
pub mod physicals;
pub mod physics;

use bevy::prelude::*;

use self::{physicals::PhysicalsPlugin, physics::PhysicsPlugin};

// Core Plugin: ------------------------------------------------------------------------------------

pub struct DynamicGravityPlugin;

impl Plugin for DynamicGravityPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugins((PhysicsPlugin, PhysicalsPlugin));
    }
}
