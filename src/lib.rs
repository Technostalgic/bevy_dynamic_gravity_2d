pub mod core;
pub mod prelude {
    pub use crate::core::physicals::{gravity_orient::*, ground_movement::*, grounded::*, *};
    pub use crate::core::physics::{dynamic_gravity::*, *};
    pub use crate::core::DynamicGravityPlugin;
}
