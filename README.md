# Bevy Dynamic Gravity 2D

Adds components that allow you to simulate dynamic gravity similar to what you'd see in Super Mario 
Galaxy. Any shape that can be a collider can also be used as a gravity attractor. Also includes 
character controller components that allow you to control a character with a sort of platformer-esq 
movement. Uses bevy_xpbd_2d, not compatible with Rapier. For a basic usage example see 
`./src/main.rs`

If you're using this package, you need to be aware of the system scheduling and system sets. It is
designed to work only with bevy_xpbd_2d used in the `FixedUpdate` schedule, which is **not** the 
default schedule for bevy_xpbdm so be aware of that, and also be sure to order your systems properly
around the system set labels included with this crate. Descriptions of what each system sets are
for, are documented in the code using rust-doc inline doc formatting.

Enjoy.

## License

License for this project is meant to be compatible with bevy's licensing. So it's dual-licensed 
under either MIT or APACHE 2.0, your choice.